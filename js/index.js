(function ($) {
    "use strict";

    /**
     * Conserve aspect ratio of the orignal region. Useful when shrinking/enlarging
     * images to fit into a certain area.
     *
     * @param {Number} srcWidth Source area width
     * @param {Number} srcHeight Source area height
     * @param {Number} maxWidth Fittable area maximum available width
     * @param {Number} maxHeight Fittable area maximum available height
     * @return {Object} { width, heigth }
     */
    var aspectRatioFit = function (srcWidth, srcHeight, maxWidth, maxHeight) {

        var wRatio = maxWidth / srcWidth,
            hRatio = maxHeight / srcHeight,
            width = srcWidth,
            height = srcHeight;

        if (srcWidth / maxWidth < srcHeight / maxHeight) {
            width = maxWidth;
            height = srcHeight * wRatio;
        } else {
            width = srcWidth * hRatio;
            height = maxHeight;
        }

        return {width: width, height: height};
    };

    $.fn.tkCover = function () {

        if (! this.length) return;

        if (this.length > 1) {
            this.each(function () {
                $(this).tkCover();
            });
            return;
        }

        var cover = this;

        if (! this.is(':visible')) return;

        var bodyPadding = {
            top: parseInt($(document.body).css('padding-top')),
            bottom: parseInt($(document.body).css('padding-bottom'))
        };
        var $contentScrollable = $('.st-content-inner');
        var $scrollable = $contentScrollable.length ? $contentScrollable : $(window);
        var wHeight = $scrollable.innerHeight() - (bodyPadding.top + bodyPadding.bottom);

        var img = cover.find('.cover-image');
        if (! img.length) img = cover.find('> img');
        if (! img.length) return;

        // select all overlays
        var overlay = cover.find('> .overlay');

        // filter only the overlays that should have the full height of the cover container
        var overlayFull = overlay.filter(function() {
            return $(this).is('.overlay-hover') || $(this).is('.overlay-full');
        });

        // reset the cover height
        cover.css('height', 'auto');

        // reset overlays height
        if (img.is('img')) {
            overlay.css('height', 'auto');
        }

        if (! this.is('[class*="height"]')) {

            var changed = false;
            var tHeight = cover.height();

            $.loadImage(img.attr('src')).done(function (image) {

                // store the current image sizes
                var oWidth = img.width();
                var oHeight = img.height();

                // reset image style
                img.removeAttr('style');

                // the new image height
                var height = img.height();

                // cover overlay sizes
                if (overlay.length) {
                    var overlayHeight = overlay.innerHeight();

                    // if the overlay is taller than the cover container
                    // increase the cover container height to the overlay's height
                    if (overlayHeight > height) {
                        height = overlayHeight;
                    }

                    // if the overlay is overlay-hover or overlay-full
                    // make sure the overlay is equally tall with the cover image
                    if (overlay.is('.overlay-hover') || overlay.is('.overlay-full')) {
                        height = img.height();
                    }
                }

                // make sure the cover is never taller than the visible viewport
                if (height > wHeight) {
                    height = wHeight;
                }

                // set the overlay height
                if (overlay.length) {
                    overlayFull.innerHeight(height);
                }

                // set the cover height
                cover.height(height);

                // set the image sizes
                img.css(aspectRatioFit(image[ 0 ].width, image[ 0 ].height, cover.width(), cover.height()));

                // notify other components that this cover is resized and ready
                cover.trigger('resize.tk.cover');

                // test for the cover or images sizes changes
                changed = tHeight !== cover.height();
                if (! changed) {
                    changed = (oWidth !== img.width() || oHeight !== img.height());
                }

                // notify other components about size changes
                if (changed) {
                    cover.trigger('changed.tk.cover');
                }
            });
        }
        else {

            // make sure the cover is never taller than the visible viewport
            if (cover.height() > wHeight) {
                cover.height(wHeight);
            }

            img.each(function () {

                var i = $(this);
                var changed = false;
                // if (i.data('autoSize') === false) return true; @TODO: DEPRECATED; FIND ITS USAGES

                var oWidth = i.width();
                var oHeight = i.height();

                if (i.is('img')) {
                    $.loadImage(i.attr('src')).done(function (img) {
                        i.removeAttr('style');
                        i.css(aspectRatioFit(img[ 0 ].width, img[ 0 ].height, cover.width(), cover.height()));
                        cover.trigger('resize.tk.cover');

                        // if the overlay is overlay-hover or overlay-full
                        // make sure the overlay is equally tall with the cover container
                        overlayFull.innerHeight(cover.height());

                        changed = (oWidth !== i.width() || oHeight !== i.height());
                        if (changed) {
                            cover.trigger('changed.tk.cover');
                        }
                    });
                }
                else {
                    i.removeAttr('style');
                    i.css(aspectRatioFit(i.width(), i.height(), cover.width(), cover.height()));
                    cover.trigger('resize.tk.cover');

                    changed = (oWidth !== i.width() || oHeight !== i.height());
                    if (changed) {
                        cover.trigger('changed.tk.cover');
                    }
                }
            });
        }

    };

    $('.cover.overlay').tkCover();

    // Bootstrap Carousels with Covers and navbar transitions
    // for navbar transitions, both the carousel and the navbar
    // have to be placed within the same parent container
    $('.carousel')
        .on('slide.bs.carousel', function (e) {

            if (! $(this).hasClass('cover')) return;

            var next = $(e.relatedTarget);
            next.find('.overlay').hide();

            if (next.data('slideNavbar')) {
                var navbar = $(this).parent().find('.navbar');
                if (navbar.length) {
                    navbar
                        .removeClass('navbar-default')
                        .removeClass('navbar-inverse')
                        .removeClass(function (index, css) {
                            return (css.match(/navbar-skin-([\S-]+)/ig) || []).join(' ');
                        })
                        .addClass(next.data('slideNavbar'));
                }
            }
        })
        .on('slid.bs.carousel', function (e) {

            if (! $(this).hasClass('cover')) return;

            var nextH = $(e.relatedTarget).height();

            $(this).animate({height: nextH}, 200, function () {

                var cover = $(this);
                cover.tkCover();

                $(this).find('.carousel-inner, .item.active').animate({
                    height: cover.height()
                }, 200);

                $(this).find('.item.active .overlay').fadeIn();
            });
        });

    var t;
    $(window).on("load debouncedresize", function () {
        clearTimeout(t);
        t = setTimeout(function () {
            $('.cover.overlay').tkCover();
        }, 200);
    });

})(jQuery);